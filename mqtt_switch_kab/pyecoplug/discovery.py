"""Discovery module for ecoplugs."""
from datetime import datetime
import struct
import socket
from threading import Thread
import time

from mqtt_switch_kab.pyecoplug.plug import EcoPlug


def normalize_string(string):
    """Normalize string received."""
    if isinstance(string, bytes):
        return string.rstrip(b' \t\r\n\0')
    return string


class EcoDiscovery():
    """Ecoplug discobery class daemon."""

    UDP_PORT = 8900

    def __init__(self, on_add, on_remove):
        """Constructor."""
        self.on_add = on_add
        self.on_remove = on_remove
        self.discovered = {}

        self.running = False

    def iterate(self):
        """Generetor for discovered plugs."""
        for plug in self.discovered.values():
            yield plug[1]

    def start(self):
        """Start discovery daemon."""
        self.running = True
        self.thread = Thread(target=self.poll_discovery)

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind(('0.0.0.0', self.UDP_PORT))
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.socket.settimeout(0.5)

        self.thread.start()

    def stop(self):
        """Stop discovery daemon."""
        self.running = False
        self.thread.join()
        for plug in self.discovered.values():
            self.on_remove(plug[1])
            plug[1].stop()
        self.discovered.clear()
        self.socket.close()

    def process_packet(self, pkt):
        """Read received packet."""
        now = time.time()
        mac_addr = pkt[-3]
        if mac_addr not in self.discovered:
            plug = EcoPlug(pkt)
            self.on_add(plug)
            self.discovered[mac_addr] = (now, plug)
        else:
            plug = self.discovered[mac_addr][1]
            plug.plug_data = pkt
            self.discovered[mac_addr] = (now, plug)

    def prune_stale(self):
        """Remove deleted plug."""
        now = time.time()
        to_remove = []
        for mac, plug in self.discovered.items():
            if now - plug[0] >= 30:
                to_remove.append(mac)
        for mac in to_remove:
            plug = self.discovered[mac][1]
            self.on_remove(plug)
            plug.stop()
            del self.discovered[mac]

    def poll_discovery(self):
        """Discovery main loop."""
        broadcast = True
        while self.running:
            if broadcast:
                last_broadcast = time.time()

                now = datetime.now()
                packet = bytearray(b'\x00' * 128)
                struct.pack_into('<HBBL', packet, 24,
                                 now.year, now.month, now.day,
                                 now.hour * 3600 + now.minute * 60 + now.second)
                self.socket.sendto(packet, ('255.255.255.255', 5888))
                self.socket.sendto(packet, ('255.255.255.255', 5888))
                self.socket.sendto(packet, ('255.255.255.255', 5888))
                self.socket.sendto(packet, ('255.255.255.255', 25))
                self.socket.sendto(packet, ('255.255.255.255', 25))
                self.socket.sendto(packet, ('255.255.255.255', 25))

                broadcast = False

            elif time.time() - last_broadcast >= 10:
                broadcast = True

            else:
                try:
                    data, _ = self.socket.recvfrom(408)
                    pkt = list(struct.unpack('<L6s32s32s32sHHBBLl64s64sH10s'
                                             '12s16s16s16sLLLLH30s18s18sL',
                                             data))
                    pkt = tuple([normalize_string(x) for x in pkt])
                    self.process_packet(pkt)

                except socket.timeout:
                    continue
                except OSError:
                    continue
                finally:
                    self.prune_stale()
