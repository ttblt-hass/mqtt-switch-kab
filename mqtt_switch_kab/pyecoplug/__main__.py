"""Entrypoint to test plug discovery."""
import time

from mqtt_switch_kab.pyecoplug.discovery import EcoDiscovery


def on_add_fct(pkt):
    """On add plug function."""
    print('Add:', repr(pkt))
    pkt.turn_on()
    print(pkt.is_on())


def on_remove_fct(pkt):
    """On remove plug function."""
    print('Remove:', repr(pkt))


if __name__ == '__main__':
    try:
        ECO = EcoDiscovery(on_add_fct, on_remove_fct)
        ECO.start()
        time.sleep(180)
    finally:
        for plug in ECO.iterate():
            plug.turn_off()
        ECO.stop()
