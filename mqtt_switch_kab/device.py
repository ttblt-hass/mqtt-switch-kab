"""Mqtt Kab switch module."""
import asyncio
import json
import os
import time

import ping3
import mqtt_hass_base
from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

from mqtt_switch_kab.pyecoplug.discovery import EcoDiscovery
from mqtt_switch_kab.pyecoplug.plug import EcoPlug
from mqtt_switch_kab.__version__ import VERSION


class MqttKab(mqtt_hass_base.MqttDevice):
    """MQTT Kab switch."""

    def __init__(self):
        """Constructor."""
        self.plug_config = []
        mqtt_hass_base.MqttDevice.__init__(self, "mqtt-kab")
        self.plugs = {}
        self.discover_new_devices = False
        self.eco_disco = None

    def _add(self, plug):
        """Add new detected switch."""
        if plug.name not in self.plugs:
            self.logger.info("New plug detected: %s %s", plug.name, str(plug.plug_data[-2]))
            self.plugs[plug.name] = plug

            # Register plug to Hass
            mqtt_base_topic = "{}/switch/{}/{}".format(self.mqtt_root_topic,
                                                       self.name,
                                                       plug.name.lower().replace("/", "_"))
            mqtt_config_topic = mqtt_base_topic + "/config"
            mqtt_command_topic = mqtt_base_topic + "/command"
            plug_config = {"name": plug.name,
                           "command_topic": mqtt_command_topic,
                           "state_topic": mqtt_base_topic + "/state",
                           "state_on": "ON",
                           "state_off": "OFF",
                           "unique_id": "--".join((self.name, plug.name.lower().replace("/", "_"))),
                           "availability_topic": mqtt_base_topic + "/availability",
                           "payload_available": "online",
                           "payload_not_available": "offline",
                           "qos": 0,
                           "optimistic": False,
                           "device": {"identifiers": plug.plug_data[4].decode('utf-8'),
                                      "connections": [["mac", plug.plug_data[-3].decode('utf-8')]],
                                      "model": plug.plug_data[2].decode('utf-8'),
                                      "name": plug.name,
                                      "sw_version": plug.plug_data[1].decode('utf-8'),
                                      }
                           }
            self.logger.info("New plug config: %s, %s", mqtt_config_topic, plug_config)
            self.mqtt_client.publish(topic=mqtt_config_topic,
                                     retain=True,
                                     payload=json.dumps(plug_config))

    def _remove(self, plug):
        """Useless function to fake remove switch."""

    def read_config(self):
        """Read env vars."""
        self.main_loop_wait_time = os.environ.get("MAIN_LOOP_WAIT_TIME", 10)
        self.search_device_time = os.environ.get("SEARCH_DEVICE_TIME", 10)
        self.discover_new_devices = bool(os.environ.get("SEARCH_NEW_DEVICE", False))
        self.config_file = os.environ.get("CONFIG", None)
        if self.config_file is not None and os.path.isfile(self.config_file):
            with open(self.config_file, 'r') as cfh:
                raw_config = load(cfh, Loader=Loader)
            print(raw_config)
            for raw_plug in raw_config:
                self.plug_config.append(
                    (None,
                     raw_plug['sw_version'].encode('utf-8'),
                     raw_plug['model'].encode('utf-8'),
                     raw_plug['name'].encode('utf-8'),
                     raw_plug['model2'].encode('utf-8'),
                     raw_plug['mac'].encode('utf-8'),
                     raw_plug['ip'].encode('utf-8'),
                     int(raw_plug.get('port', 80)))
                )

    def _on_connect(self, client, userdata, flags, rc):
        """MQTT on connect callback."""
        if self.discover_new_devices:
            self.update_device_list()
        for data in self.plug_config:
            plug = EcoPlug(data)
            self._add(plug)

    def _on_publish(self, client, userdata, mid):
        """MQTT on publish callback."""

    def _mqtt_subscribe(self, client, userdata, flags, rc):
        """Subscribe to all needed MQTT topic."""
        # Subscribe to all switch inputs
        for plug in self.plugs.values():
            mqtt_base_topic = "{}/switch/{}/{}".format(self.mqtt_root_topic,
                                                       self.name,
                                                       plug.name.lower().replace("/", "_"))
            mqtt_command_topic = mqtt_base_topic + "/command"
            self.logger.info("Subscribe to %s", mqtt_command_topic)
            client.subscribe(mqtt_command_topic)

        if self.discover_new_devices:
            mqtt_update_topic = "{}/switch/{}/update_device_list".format(self.mqtt_root_topic,
                                                                         self.name)
            self.logger.info("Subscribe to %s", mqtt_update_topic)
            scan_config = {"name": self.name + "-scan",
                           "command_topic": mqtt_update_topic,
                           "qos": 0,
                           "optimistic": True,
                           "device": {"identifiers": self.name,
                                      "name": self.name,
                                      "sw_version": VERSION,
                                      }
                           }

            self.logger.info("Send scanner config: %s, %s",
                             mqtt_update_topic + "/config",
                             scan_config)
            # UnSubscribe to avoid loop calls because of the next publish command
            client.unsubscribe(mqtt_update_topic)
            self.mqtt_client.publish(topic=mqtt_update_topic + "/config",
                                     retain=True,
                                     payload=json.dumps(scan_config))
            # Subscribe to update device command
            client.subscribe(mqtt_update_topic)

    def _on_message(self, client, userdata, msg):
        """MQTT on message callback."""
        self.logger.debug("MESSAGE received: %s : %s", msg.topic, msg.payload)
        topic_splitted = msg.topic.split("/")
        plug_name = topic_splitted[-2]
        subtopic = topic_splitted[-1]
        if subtopic == "update_device_list" and self.discover_new_devices:
            if msg.payload == b'ON':
                self.logger.info("Search for new devices and delete old ones")
                self.update_device_list()
            return
        if plug_name not in self.plugs:
            self.logger.error('Plug %s not found', plug_name)
            return
        if subtopic == 'command':
            plug = self.plugs[plug_name]
            if msg.payload == b'ON':
                plug.turn_on()
            elif msg.payload == b'OFF':
                plug.turn_off()
            else:
                self.logger.error('Plug state not supported: %s', msg.payload)
                return
            self.get_current_state(plug)

    def _signal_handler(self, signal_, frame):
        """Handle SIGKILL."""

    def get_current_state(self, plug):
        """Get current plug state and send it on MQTT."""
        mqtt_base_topic = "{}/switch/{}/{}".format(self.mqtt_root_topic,
                                                   self.name,
                                                   plug.name.lower().replace("/", "_"))
        # STATE
        if plug.is_on():
            state = "ON"
        else:
            state = "OFF"
        self.logger.debug("%s is %s", plug.name, state)
        mqtt_state_topic = mqtt_base_topic + "/state"
        self.mqtt_client.publish(topic=mqtt_state_topic, payload=state)

    def update_device_list(self):
        """Scan for new device and delete non reachable ones."""
        if self.discover_new_devices is False:
            return
        old_plugs = self.plugs.copy()
        self.logger.info("Searching for devices...")
        self.eco_disco = EcoDiscovery(self._add, self._remove)
        self.eco_disco.start()
        i = 0
        while i < self.search_device_time and self.must_run:
            time.sleep(1)
            i += 1

        for plug in old_plugs.values():
            if plug.name not in [d[1].name for d in self.eco_disco.discovered.values()]:
                self.logger.info("Removing old plug: %s", plug.name)
                # Remove old plug from Home Assistant
                mqtt_base_topic = "{}/switch/{}/{}".format(self.mqtt_root_topic,
                                                           self.name,
                                                           plug.name.lower().replace("/", "_"))
                mqtt_config_topic = mqtt_base_topic + "/config"
                self.mqtt_client.publish(topic=mqtt_config_topic,
                                         retain=True,
                                         payload=json.dumps({}))
                self.logger.info("Old plug removed from Home Assistant: %s", plug.name)
                # Stop old plug
                plug.stop()

        # Stop discovery daemon
        self.eco_disco.running = False
        self.eco_disco.discovered.clear()
        self.eco_disco.socket.close()

        self.eco_disco = None

    async def _init_main_loop(self):
        """Init before starting main loop."""

    async def _main_loop(self):
        """Run main loop."""
        self.logger.debug("Plug list: %s", str(self.plugs.values()))
        try:
            for plug in self.plugs.values():
                mqtt_base_topic = "{}/switch/{}/{}".format(self.mqtt_root_topic,
                                                           self.name,
                                                           plug.name.lower().replace("/", "_"))

                mqtt_availability_topic = mqtt_base_topic + "/availability"
                ping_res = ping3.ping(plug.plug_data[-2], timeout=3)
                if not ping_res:
                    state = "offline"
                    self.mqtt_client.publish(topic=mqtt_availability_topic, payload=state)
                    continue
                # ONLINE
                self.logger.debug("%s is online", plug.name)
                state = "online"
                self.mqtt_client.publish(topic=mqtt_availability_topic, payload=state)

                # STATE
                self.get_current_state(plug)

        except RuntimeError as exp:
            self.logger.warning(exp)

        i = 0
        while i < self.main_loop_wait_time and self.must_run:
            await asyncio.sleep(1)
            i += 1

    async def _loop_stopped(self):
        """Run after the end of the main loop."""
        if self.eco_disco:
            self.eco_disco.stop()
        for plug in self.plugs.values():
            # OFFLINE
            self.logger.debug("stopping %s", plug.name)
            state = "offline"
            mqtt_base_topic = "{}/switch/{}/{}".format(self.mqtt_root_topic,
                                                       self.name,
                                                       plug.name.lower().replace("/", "_"))

            mqtt_availability_topic = mqtt_base_topic + "/availability"

            self.mqtt_client.publish(topic=mqtt_availability_topic, payload=state)
            plug.stop()
