## Run it locally

Run it:
```
pip install -r requirements.txt

MQTT_USERNAME=hass \
	MQTT_PASSWORD=hass \
	MQTT_HOST=192.168.0.1 \
	MQTT_PORT=1883 \
	ROOT_TOPIC=homeassistant \
	MQTT_NAME=kabswitches \
	LOG_LEVEL=DEBUG \
	env/bin/mqtt_switch_kab

```

## Build Docker image

```
docker build -t mqtt-switch-kab .
```

## Run Docker container

```
docker run --rm -it \
    --name bttest \
    --privileged --net=host \
    -e DEVICE=/dev/ttyUSB1 \
    -e DATABASE=/data/app.db \
    -e BAUDRATE=57600 \
    -e MQTT_USERNAME=hass \
    -e MQTT_PASSWORD=hass \
    -e MQTT_HOST=192.168.0.1 \
    -e MQTT_PORT=1883 \
    -e ROOT_TOPIC=homeassistant \
    -e LOG_LEVEL=INFO \
    -v `pwd`/data:/data \
    mqtt-switch-kab:latest bash
```


# Links

* https://github.com/blakewford/kab
