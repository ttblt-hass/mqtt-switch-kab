import sys

from setuptools import setup, find_packages
from mqtt_switch_kab.__version__ import VERSION

if sys.version_info < (3,7):
    sys.exit('Sorry, Python < 3.7 is not supported')

install_requires = list(val.strip() for val in open('requirements.txt'))
tests_require = list(val.strip() for val in open('test_requirements.txt'))

setup(name='mqtt-switch-kab',
      version=VERSION,
      description=('Daemon managing KAB switches with '
                   'Home-Assistant through MQTT'),
      author='Thibault Cohen',
      author_email='titilambert@gmail.com',
      url='http://gitlab.com/titilambert/mqtt_switch_kab',
      package_data={'': ['LICENSE.txt']},
      include_package_data=True,
      packages=find_packages(),
      entry_points={
          'console_scripts': [
              'mqtt_switch_kab = mqtt_switch_kab.__main__:main',
          ]
      },
      license='Apache 2.0',
      install_requires=install_requires,
      tests_require=tests_require,
      classifiers=[
        'Programming Language :: Python :: 3.7',
      ]
)
